// gotamer/conv converts between []byte int64

package conv

// Converts
/***************************************************

	[]byte to int64

	int64 to []byte

*****************************************************/

// int64 to Byte Slice
func Itob(in int64) []byte {
	return []byte{
		byte(in >> 56),
		byte(in >> 48),
		byte(in >> 40),
		byte(in >> 32),
		byte(in >> 24),
		byte(in >> 16),
		byte(in >> 8),
		byte(in),
	}
}

// Byte Slice to int64
func Btoi(buf []byte) int64 {
	return int64(buf[7]) |
		int64(buf[6])<<8 |
		int64(buf[5])<<16 |
		int64(buf[4])<<24 |
		int64(buf[3])<<32 |
		int64(buf[2])<<40 |
		int64(buf[1])<<48 |
		int64(buf[0])<<56
}

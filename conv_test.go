package conv

import "testing"

func TestConv(t *testing.T) {

	var i int64 = 9223372036854775807
	var b []byte

	b = Itob(i)
	ii := Btoi(b)
	if i != ii {
		t.Errorf("%v should equal %v", i, ii)
	}
}
